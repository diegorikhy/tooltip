angular.module 'appAngular', [
  'sc.app.helpers'
]

.config [
  '$locationProvider'
  ($locationProvider)->
    $locationProvider.html5Mode
      enabled: true
      requireBase: false
]


.run [
  '$rootScope'
  '$window'
  'scTopMessages'
  'scAlert'
  ($rootScope, $window, scTopMessages, scAlert)->
    $rootScope.scTopMessages = scTopMessages
    $rootScope.scAlert = scAlert
    $rootScope.$baseUrlCliente = '/'

    $rootScope.checkMobileness = ->
      $rootScope.isMobile = ($window.innerWidth < 480)
      $rootScope.screen =
        isXs: $rootScope.isMobile
        isSm: (479 < $window.innerWidth < 960)
        isMd: (959 < $window.innerWidth < 1280)
        isLg: (1279 < $window.innerWidth)

    $rootScope.checkMobileness()

    angular.element($window).bind 'resize', ->
      $rootScope.checkMobileness()
      $rootScope.$apply()
]
