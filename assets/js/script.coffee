angular.module 'app', [
  'appAngular'
]

.controller 'Uikit::ScTooltip', [
  '$scope'
  ($s)->
    $s.positions = [
      'top'
      'left'
      'bottom'
      'right'
      'top-left'
      'top-right'
      'bottom-left'
      'bottom-right'
    ]

    $s.sizes = [
      'sm'
      'md'
      'lg'
    ]

    $s.classes = ->
      classes = []
      if $s.useScTooltipPosition
        classes.push "sc-tooltip-#{$s.scTooltipPosition}"
      if $s.useScTooltipSize
        classes.push "sc-tooltip-#{$s.scTooltipSize}"
      if $s.useScTooltipAlways
        classes.push 'sc-tooltip-always'
      classes.join ' '

    $s.conteudo = 'Lorem Ipsum is simply dummy text of the printing.'
]
